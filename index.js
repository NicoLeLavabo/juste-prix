/**
 * Get random number between min max
 * @author Nicolas
 * @param {entier} min 
 * @param {entier} max 
 * @return random result beetween between min max
 */
function randomMinMax(min, max) {
    let result = Math.round(Math.random() * (max - min) + min);
    return result;
}

/**
 * Get number chosen by the user
 * @author Nicolas
 * @return number chosen by the user in prompt
 */
function demande() {
    return prompt('Quel est votre nombre ?');
}

/**
 * Principal function of this game, show informations with alert()
 * @author Nicolas
 *
 */
function justePrix() {
    let result = randomMinMax(1, 100);
    console.log(result);
    let chance = 10;
    let trouver = false;
    while (trouver == false && chance !== 0) {
        chance--;
        let essai = demande();
        if (isNaN(essai)) {
            alert('Attention vous devez entrer un nombre, la partie est annulée');

        }
        else if (chance == 0) {
            alert('vous n\'avez plus de chance, le nombre à trouver était de ' + result);
        }
        else if (essai > result) {
            alert('le nombre demandé est plus petit, il vous reste ' + chance + ' chances');
        }
        else if (essai < result) {
            alert('le nombre demandé est plus grand, il vous reste ' + chance + ' chances');
        }
        else if (essai == result) {
            trouver = true;
            alert('Bravo vous avez gagné et il vous restait ' + chance + ' chances');
        }
    }
}